# Copyright 2016 Julien Pivotto <roidelapluie@inuits.eu>
# Distributed under the terms of the GNU General Public License v2

require github [ user=SELinuxProject tag="${PNV}" pn=selinux ] \
    python [ blacklist=none multibuild=true with_opt=true multiunpack=true work="selinux-${PNV}/${PN}" ]

SUMMARY="SELinux runtime shared libraries"
DESCRIPTION="
Shared libraries used by SELinux that are used by SELinux-aware applications to get and set process
and file security contexts and to obtain security policy decisions.
"

BUGS_TO="roidelapluie@inuits.eu"

LICENCES="public-domain"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="python"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/pcre
        dev-libs/ustr
        security/libsepol[~${PV}]
        python? ( dev-lang/swig )
"

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    SBINDIR=/usr/$(exhost --target)/bin
    SHLIBDIR=/usr/$(exhost --target)/lib
    MAN3DIR=/usr/share/man/man3
    MAN5DIR=/usr/share/man/man5
    MAN8DIR=/usr/share/man/man8
)

compile_one_multibuild() {
    default

    option python && emake pywrap \
        PYTHON="${PYTHON}"
}

install_one_multibuild() {
    default

    option python && emake install-pywrap \
        "${DEFAULT_SRC_INSTALL_PARAMS[@]}" \
        PYTHONLIBDIR="${IMAGE}/$(python_get_sitedir)" \
        PYTHON="${PYTHON}"
}

